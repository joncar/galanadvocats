<!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

    <nav id="mobile-advanced" class="mobile-advanced"></nav>

    <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

    <!-- ContactPopup -->

      <div class="contactform-wrap">
        <div class="vc-child h-inherit">

          <div class="team-desc overlay style-2">
              <div class="member-info">
                <div class="flex-row flex-justify flex-center">
                  <div class="text-size-medium">        
                    <div class="content-element5">                                    
                      <p>Deixa'ns la teva consulta i contactarem amb tu al més aviat possible.</p>
                    </div>
                 
                    [contactForm]
                  </div>
                </div>
              </div>
            </div>

            

          <button class="close-search-form"></button>

        </div>
      </div>

    <header id="header" class="header sticky-header">

      <!-- searchform -->

      <div class="searchform-wrap">
        <div class="vc-child h-inherit">

          <form>
            <button type="submit" class="search-button"></button>
            <div class="wrapper">
              <input type="text" name="search" placeholder="Start typing...">
            </div>
          </form>

          <button class="close-search-form"></button>

        </div>
      </div>

      

      <div class="flex-row flex-center flex-justify">

        <div class="top-header flex-row flex-center">
          
          <!-- logo -->
        
          <div class="logo-wrap">
          
            <a href="<?= site_url() ?>" class="logo"><img src="<?= base_url() ?>theme/images/logo.jpg" alt=""></a>
          
          </div>
          
          <!-- - - - - - - - - - - - / Mobile Menu - - - - - - - - - - - - - -->
          
          <!--main menu-->
          
          <div class="menu-holder flex-row flex-justify">
            
            <div class="menu-wrap">
          
              <div class="nav-item flex-row flex-justify flex-center">
                
                <!-- - - - - - - - - - - - - - Navigation - - - - - - - - - - - - - - - - -->
        
                <nav id="main-navigation" class="main-navigation">
                  <?php $this->load->view('includes/template/menu'); ?>
                </nav>
        
                <!-- - - - - - - - - - - - - end Navigation - - - - - - - - - - - - - - - -->
        
                <div class="search-holder">
                
                  <button type="button" class="search-button"></button>
        
                </div>
        
              </div>
          
            </div>

          </div>

        </div>

        <!-- Contact info -->

        <ul class="contact-info">
          <li class="info-item">
            <button type="button" class="btn btn-big btn-style-5 consulta">Consulta</button>
          </li>
          <li class="info-item" style="padding-top: 13px;">
            <i class="licon-telephone"></i>
            <div class="item-info">
              <span content="telephone=no">938 031 869</span>
            </div>
          </li>
        </ul>

      </div>
      
    </header>