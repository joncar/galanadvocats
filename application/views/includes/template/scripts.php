<!-- JS Libs & Plugins
  ============================================ -->
  <script src="<?= base_url() ?>theme/js/libs/jquery.modernizr.js"></script>
  <script src="<?= base_url() ?>theme/js/libs/jquery-2.2.4.min.js"></script>
  <script src="<?= base_url() ?>theme/js/libs/jquery-ui.min.js"></script>
  <script src="<?= base_url() ?>theme/js/libs/retina.min.js"></script>
  <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc&libraries=drawing,geometry,places"></script> 
  <script src="<?= base_url() ?>theme/plugins/isotope.pkgd.min.js"></script>
  <script src="<?= base_url() ?>theme/plugins/jquery.queryloader2.min.js"></script>
  <script src="<?= base_url() ?>theme/plugins/owl.carousel.min.js"></script>
  <script src="<?= base_url() ?>theme/plugins/revolution/js/jquery.themepunch.tools.min.js?ver=5.0"></script>
  <script src="<?= base_url() ?>theme/plugins/revolution/js/jquery.themepunch.revolution.min.js?ver=5.0"></script>

  <!-- JS theme files
  ============================================ -->
  <script src="<?= base_url() ?>theme/js/plugins.js"></script>
  <script src="<?= base_url() ?>theme/js/script.js"></script>
  <script>
    $(document).on('ready',function(){
      $(".consulta").on('click',function(){
        $(".contactform-wrap").addClass('opened');
        
      });

      $(document).on('click','a',function(e){
          var element = $(this).attr('href');
          if(element.indexOf('#')==0){
              if($(element).length>0){
                e.preventDefault();
                $('html,body').animate({scrollTop:$(element).offset().top},600);
              }
          }
      });
    });
    
  </script>