<!doctype html>
<html lang="<?= $_SESSION['lang'] ?>">

	<!-- Google Web Fonts
	================================================== -->

	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i%7CFrank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

  <title><?= empty($title) ? 'Monalco' : $title ?></title>
  <meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
  <meta name="description" content="<?= empty($keywords) ?'': $description ?>" /> 

	<!-- Mobile Specific Metas
	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!-- Vendor CSS
	============================================ -->
	
	<link rel="stylesheet" href="<?= base_url() ?>theme/font/demo-files/demo.css">
    <link rel="stylesheet" href="<?= base_url() ?>theme/plugins/revolution/css/settings.css">
    <link rel="stylesheet" href="<?= base_url() ?>theme/plugins/revolution/css/layers.css">
    <link rel="stylesheet" href="<?= base_url() ?>theme/plugins/revolution/css/navigation.css">

	<!-- CSS theme files
	============================================ -->
	<link rel="stylesheet" href="<?= base_url() ?>theme/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>theme/css/fontello.css">
	<link rel="stylesheet" href="<?= base_url() ?>theme/css/owl.carousel.css">
	<link rel="stylesheet" href="<?= base_url() ?>theme/css/style.css">
	<link rel="stylesheet" href="<?= base_url() ?>theme/css/responsive.css">
  <link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">
  <script>
  	var URL = '<?= base_url() ?>';
  </script>
</head>

<body>
<?php if($this->router->fetch_method()!='editor'): ?>
  <div class="loader"></div>
<?php endif ?>

  <div id="wrapper" class="wrapper-container">
  	<?php $this->load->view($view); ?>
  </div>

  <!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->
  <?php if($this->router->fetch_method()!='editor') $this->load->view('includes/template/scripts') ?>
  
  <script src="https://www.google.com/recaptcha/api.js?render=6LfD2q4UAAAAAIO-cbG_O6SRahpxOFii_zBgz-qq"></script>
  <script>
  grecaptcha.ready(function() {
      grecaptcha.execute('6LfD2q4UAAAAAIO-cbG_O6SRahpxOFii_zBgz-qq', {action: 'homepage'}).then(function(token) {
      	console.log(token);
         $(document).find("input[name='token']").val(token);
      });
  });
  </script>
</body>
</html>