<?php

class Multicrud{
    public $cruds = array();
    protected $post = array();
    protected $ids = array();
    protected $primary = '';
    public function addCrud($crud){
        array_push($this->cruds,$crud);
    }
    public function render(){
        if(!empty($_POST)){
            $this->post = $_POST;
        }        
        $output = array();
        $css_files = array();
        $js_files = array();
        $primary = $this->cruds[0]->getStateInfo();
        if(!empty($primary->primary_key)){
            $primary = $primary->primary_key;
            $this->primary = $primary;
        }
        foreach($this->cruds as $c){
                $norequireds = array();
                if(!empty($c->dependency)){
                    foreach($c->dependency as $v=>$d){
                        if(get_class($c)=='ajax_grocery_CRUD'){                            
                            $c->field_type($v,'hidden',$d);
                            $c->set_primary_key($v);
                            array_push($norequireds,$v);
                            $c->norequireds = $norequireds;                            
                            $c->required_fields_array();
                        }else{
                            foreach($c->dependency as $n=>$d){
                                $c->set_where($n,0);
                                if($this->cruds[0]->getParameters()!='add' && $this->cruds[0]->getParameters()!='insert_validation' && $this->cruds[0]->getParameters()!='insert'){
                                    if(!empty($this->primary) || !empty($_SESSION['image_crud_relation_field'])){
                                        if(empty($this->primary)){
                                            $primary = $_SESSION['image_crud_relation_field'];
                                        }else{
                                            $primary = $this->primary;
                                        }
                                        $c->set_or_where($n,$primary);
                                        $_SESSION['image_crud_relation_field'] = $primary;
                                    }
                                }else{
                                    unset($_SESSION['image_crud_relation_field']);
                                }
                            }
                        }
                    }
                }                            
            $c->display_and_exit = 'noactivo';
            if(!empty($this->post)){
                $c = $this->prepare_post($c);                
            }else{
                $c = $c->render();                
            }
            if($this->cruds[0]->getParameters()=='list'){
                $css_files = $c->css_files;
                $js_files = $c->js_files;
            }else{
                foreach($c->css_files as $cc){
                    if(!in_array($cc, $css_files)){
                        $css_files[] = $cc;
                    }
                }
                foreach($c->js_files as $cc){                
                    if(!in_array($cc,$js_files)){
                        $js_files[] = $cc;
                    }
                }
            }
            array_push($output,$c);
        }
        if(!empty($this->post)){
            die();
        }
        return (object)array('output'=>$output,'css_files'=>$css_files,'js_files'=>$js_files);
    }
    protected function prepare_post($c){
        //Si el crud es el primario, se envian los datos sin los array
        if(empty($c->dependency)){
            $table = $c->get_table();
            $c = $c->render();            
            //return (object)array('css_files'=>array(),'js_files'=>array());
            $this->ids[$table] = get_instance()->table_insert;            
            return $c;
        }else{
            //Si es un crud dependiente se transforma sus arrays
            if(get_class($c)=='ajax_grocery_CRUD'){
                $table = $c->get_table();
                $fields = get_instance()->db->field_data($table);
                $exit = false;
                if($c->getParameters()=='edit' || $c->getParameters()=='update' || $c->getParameters()=='update_validation'){
                    //Comparar las filas que llegaron
                    foreach($c->dependency as $v=>$d){
                        get_instance()->db->delete($table,array($v=>$this->primary));
                    }
                    $this->ids[$this->cruds[0]->get_table()] = $this->primary;
                }
                //Ver quien es el primer elemento que esta en el form
                switch($c->getParameters(false)){
                    case 'insert': 
                    case 'update': $state = 5; break;
                    case 'insert_validation': 
                    case 'update_validation': $state = 9; break;
                    default: $state = 5; break;
                }
                for($i=0;$i<=$this->post['filas_'.$c->get_table()];$i++){                    
                        $_POST = array();                                  
                        foreach($fields as $f){
                            if($f->name!='id' && !array_key_exists($f->name,$c->dependency)){
                                $_POST[$f->name] = $this->post[$f->name.'_'.$i.'_'.$table];                            
                            }
                            elseif(!empty($this->post['id_'.$i.'_'.$table])){
                                $_POST['id'] = $this->post['id_'.$i.'_'.$table];
                            }
                        }
                        //llenar dependencias
                        
                        foreach($c->dependency as $n=>$d){
                            list($tablename,$cell) = explode('.',$d);
                            $_POST[$n] = $this->ids[$tablename];                            
                        }
                        
                        if (ob_get_contents()){
                            ob_clean();
                        }                          
                        $render = $c->render($state);                    
                }
                foreach($c->dependency as $v=>$d){
                    get_instance()->db->delete($table,array($v=>0));
                }
                return $render;
            }else{                
                echo $this->primary;
                foreach($c->dependency as $n=>$d){
                    list($tablename,$cell) = explode('.',$d);
                    get_instance()->db->update($c->get_table(),array($n=>$this->ids[$tablename]),array($n=>0));
                }
                return $c->render();
            }
        }
        return false;
    }
}
