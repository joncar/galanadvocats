<!-- WELCOME -->
<section id="content">
    <?php if(empty($content)): ?>
	<div class="welcome">
            <div class="container">
                <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                    <div class="row">
                        <div class="title">
                            <h6>welcome</h6>
                            <h3>The Hotel at a Glance</h3>
                            <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                                <span><img alt="" src="<?= base_url() ?>img/cap.png" class="img-responsive"></span>
                            </div>
                            <p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="left-welcome">
                            <h2>The Perfect Place to Escape</h2>
                            <p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti eu sociosqu ad litora torquent conubia per inceptos.</p>
                            <p>Mauris in erat justo nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum auctor fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="right-welcome">
                            <div id="promovid">
                                <div class="custom-th">
                                    <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/video.png" class="img-responsive" alt=""/>    
                                </div>
                                <div id="thevideo" style="display:none; padding: 0px;">
                                    <video preload="auto">
                                        <source src="http://iurevych.github.com/Flat-UI-videos/big_buck_bunny.mp4"/>
                                    </video>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php else: ?>
        <?= $content ?>
    <?php endif ?>
</section>