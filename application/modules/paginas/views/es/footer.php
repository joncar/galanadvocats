<footer id="footer" class="footer style-2">

      <div class="container">
        
        <div class="main-footer">
          
          <div class="row flex-row">
          
            <div class="col-md-5 col-sm-12">
              
              <div class="widget">
                
                <a href="<?= base_url() ?>" class="logo"><img src="<?= base_url() ?>theme/images/logo2.png" alt=""></a>

                <div class="content-element4">
                  
                  <ul class="contact-info style-2">
                    <li class="info-item">
                      <div class="pre">Llámanos hoy</div>
                      <div class="item-info">
                        <span content="telephone=no">938 031 869</span>
                      </div>
                    </li>
                  </ul>

                </div>

                <p>
                    Passeig Jacint Verdaguer, 110. 1r, 2a. 08700 IGUALADA. <br>
                  <span>Teléfono:</span> 938 031 869 <br>
                  <span>Fax:</span> 938 066 062 <br>
                  <span>Email:</span><a href="mailto:info@galanadvocats.com" style="color:#fff"> info@galanadvocats.com</a> <br>
                  <span>Horarios:</span>  Lun - Jue 9h a 13h y de 16h a 20h / Vie 9h a 13h
                </p>

                <a href="#" class="info-btn">Encuentra la dirección</a>

              </div>

            </div>
            <div class="col-md-3 col-sm-12">
              <div class="widget">
                <h6 class="widget-title">Links</h6>
                <ul class="info-links v-type">
                  <li><a href="<?= site_url() ?>">Inici</a></li>
                  <li><a href="<?= site_url('p/nosaltres') ?>">Nosotros</a></li>
                  <li><a href="<?= site_url('serveis') ?>">Servicios</a></li>
                  <li><a href="<?= site_url('blog') ?>">Blog</a></li>
                  <li><a href="<?= site_url('p/contacte') ?>">Contacto</a>
                  <li><a href="<?= site_url('p/avis-legal') ?>">Aviso Legal</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-4 col-sm-12">
              <div class="widget">
                
                <h6 class="widget-title">Quieres estar informado?</h6>

                <form id="newsletter" class="newsletter">
                  <input type="email" name="newsletter-email" placeholder="email">
                  <button type="submit" data-type="submit"><i class="licon-envelope"></i></button>
                </form>

              </div>

              <div class="widget">
                
                <h6 class="widget-title">Redes Sociales</h6>

                <ul class="social-icons">

                  <li><a href="#"><i class="icon-facebook"></i></a></li>

                </ul>

              </div>

            </div>

          </div>

        </div>

        <div class="copyright">
          <p>Copyright Galan Advocats © 2018. Todos los derechos reservados.</p>
        </div>

      </div>

    </footer>