<form id="contact-form" class="contact-form" method="post" action="<?= base_url('paginas/frontend/contacto') ?>">
              
  <div class="flex-row">
    
    <div class="col-sm-4">
      
      <input type="text" name="nombre" placeholder="Nom y Apellidos *" required>
    </div>
    <div class="col-sm-4">
      
      <input type="tel" name="telefono" placeholder="Telèfon *" required>
    </div>
    <div class="col-sm-4">
      
      <input type="email" name="email" placeholder="Email *" required>
    </div>
    <div class="col-sm-12">
      
      <textarea rows="3" name="message" placeholder="Mensaje *" required></textarea>
    </div>
    <div class="col-sm-12">
      <input type="hidden" id="token" name="token" value="">
      <button type="submit" class="btn btn-style-2" data-type="submit">Enviar</button>
    </div>
  </div>
</form>