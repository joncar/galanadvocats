<!-- Chat button -->

<!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->
[header]
<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->
<!-- WELCOME -->
<section id="content">
  <div id="content" class="page-content-wrap2">
    
    <div class="container">
      
      <div class="row">
        
        <main id="main" class="col-md-8 col-sm-12"><!-- entry -->
        <div class="entry single-entry">
          
          <!-- - - - - - - - - - - - - - Entry attachment - - - - - - - - - - - - - - - - -->
          <div class="thumbnail-attachment">
            <a href="[link]"><img src="[foto]" alt=""></a>
          </div>
          
          <!-- - - - - - - - - - - - - - Entry body - - - - - - - - - - - - - - - - -->
          <div class="entry-body">
            
            <div class="label">
              <div class="title">Featured</div>
              <div class="date">
                <h6 class="month">[mes]</h6>
                <h4 class="day">[dia]</h4>
              </div>
              <span class="icon licon-news"></span>
            </div>
            <div class="wrapper">
              
              <div class="entry-meta">
                by <a href="#" class="entry-cat">[user]</a>
                <a href="#" class="entry-cat">news</a>
                <a href="#" class="entry-cat">[comentarios] comments</a>
              </div>
              <div class="content-element5">
                [texto]
              </div>
              
            </div>
          </div>
        </div>
        <!--<div class="content-element2">
          
          <div class="entries-nav">
            <div class="flex-row flex-justify">
              
              <div class="prev">
                
                <a href="#" class="info-btn with-icon left-side">Previous Post <i class="licon-arrow-left"></i></a>
                <h6>Quisque Diam Lorem</h6>
              </div>
              <div class="next">
                
                <a href="#" class="info-btn with-icon">Next Post <i class="licon-arrow-right"></i></a>
                <h6>Aliquam Dapibus Tincidunt</h6>
              </div>
            </div>
          </div>
        </div>-->

</main>
[aside]
</div>
</div>
</div>    </section>