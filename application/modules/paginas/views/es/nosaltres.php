<!-- Chat button -->

    

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

    [header]

    <!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap">

      <div class="container">
        
        <h1 class="page-title">Nosotros</h1>

        <ul class="breadcrumbs">

          <li><a href="index.html">Inici</a></li>
          <li>Nosotros</li>

        </ul>

      </div>

    </div>
<div id="content">

      <!-- Page section -->
      <div class="page-section half-bg-col">

        <div class="img-col-left"><div class="col-bg" data-bg="<?= base_url() ?>theme/images/grupo.jpg"></div></div>
        
        <div class="container">
          <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
              
              <h2 class="section-title">Quién somos Galan Advocats<span class="item-divider-3 style-3"></span></h2>

              <p>Galán Advocats es un despacho que ofrece sus servicios de asesoramiento jurídico integral desde el año 2000, ofreciendo asesoramiento legal y defensa jurídica a particulares y empresas. La satisfacción de nuestros clientes es la mejor prueba del trabajo muy hecho.
  </p>
              <p>Galan Advocats es la garantía que su caso está en buenas manos. Nuestra profesionalidad, rigor, dedicación y proximidad harán que, desde el primer momento, usted tome conciencia que sus intereses serán bien defendidos.
</p>

              <a href="[base_url]#equip" class="btn">Conoce nuestro equipo</a>

            </div>
          </div>
        </div>

      </div>

      <!-- Page section -->
      <div class="page-section-bg2 half-bg-col">

        <div class="img-col-right"><div class="col-bg" data-bg="<?= base_url() ?>theme/images/960x644_img2.jpg"></div></div>
        
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              
              <h2 class="section-title">Nuestros Valores<span class="item-divider-3 style-3"></span></h2>

              <div class="icons-box style-2">
              
                <div class="row flex-row">
              
                  <div class="col-sm-12">
                    
                    <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->        
                    <div class="icons-wrap">
              
                      <div class="icons-item">
                        <div class="item-box">
                          <i class="licon-balance"></i>
                          <h5 class="icons-box-title"><a href="#">Experiencia</a></h5>
                          <p>Los años de trabajo avalan nuestra experiencia, nuestros resultados lo confirman. </p>
                        </div>
                      </div>
              
                    </div>
              
                  </div>
                  <div class="col-sm-12">
                    
                    <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->        
                    <div class="icons-wrap">
              
                      <div class="icons-item">
                        <div class="item-box">
                          <i class="licon-man"></i>
                          <h5 class="icons-box-title"><a href="#">Integridad</a></h5>
                          <p>Honradez, honestidad y responsabilidad, la principal razón para confiar en nosotros.</p>
                        </div>
                      </div>
              
                    </div>
              
                  </div>
                  <div class="col-sm-12">
                    
                    <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->        
                    <div class="icons-wrap">
              
                      <div class="icons-item">
                        <div class="item-box">
                          <i class="licon-briefcase"></i>
                          <h5 class="icons-box-title"><a href="#">Dedicación</a></h5>
                          <p>Trabajamos por tú, directamente y en constante comunicación.</p>
                        </div>
                      </div>
              
                    </div>
              
                  </div>
                    
                </div>
              
              </div>

            </div>
            <div class="col-md-6"></div>
          </div>
        </div>

      </div>

      <!-- Page section -->
      <!-- 
<div class="page-section type-2">
        
        <div class="container">

          <div class="row counter-wrap">

            <div class="col-lg-3 col-md-3 col-xs-6">

              <div class="counter">
                <div class="count-item">
                  <span class="licon-hammer2"></span>
                  <h3 class="timer count-number" data-to="50" data-speed="1500">0</h3>
                </div>
                <p>Años de experiencia</p>
              </div>

            </div>
            <div class="col-lg-3 col-md-3 col-xs-6">

              <div class="counter">
                <div class="count-item">
                  <span class="licon-happy"></span>
                  <h3 class="timer count-number" data-to="389" data-speed="1500">0</h3>
                </div>
                <p>Clientes satisfechos</p>
              </div>

            </div>
            <div class="col-lg-3 col-md-3 col-xs-6">

              <div class="counter">
                <div class="count-item">
                  <span class="licon-group-work"></span>
                  <h3 class="timer count-number" data-to="15" data-speed="1500">0</h3>
                </div>
                <p>Nuestro equipo</p>
              </div>

            </div>
            <div class="col-lg-3 col-md-3 col-xs-6">

              <div class="counter">
                <div class="count-item">
                  <span class="licon-trophy2"></span>
                  <h3 class="timer count-number" data-to="227" data-speed="1500">0</h3>
                </div>
                <p>Casos resueltos positivamente</p>
              </div>

            </div>

          </div>

        </div>

      </div>
 -->

      <!-- Page section -->
      <div class="page-section half-bg-col two-cols">

        <div class="img-col-left"><div class="col-bg" data-bg="<?= base_url() ?>theme/images/960x616_bg1.jpg"></div></div>

        <div class="img-col-right"><div class="col-bg" data-bg="<?= base_url() ?>theme/images/960x560_bg3.jpg"></div></div>
        
        <div class="container extra">
          <div class="row">
            <div class="col-md-6 col-sm-12">
              
              <h2 class="section-title">Servicios Civiles<span class="item-divider-3 style-3"></span></h2>

              <p class="section-text">Dentro del derecho Civil tratamos diferente áreas <br>  <br>  </p>

              <div class="row">
                <div class="col-sm-6">
                  
                  <ul class="custom-list type-1">

                    <li><a href="#">Reclamaciones a seguros</a></li>
                    <li><a href="#">Accidentes de tráfico</a></li>
                    <li><a href="#">Negligencias médicas</a></li>

                  </ul>

                </div>
                <div class="col-sm-6">
                  
                  <ul class="custom-list type-1">

                    <li><a href="#">Derecho bancario</a></li>
                    <li><a href="#">Inmobiliaria y construcción</a></li>
                    <li><a href="#">Arrendamientos</a></li>

                  </ul>

                </div>
              </div>

            </div>
            <div class="page-section-bg2 green col-md-6 col-sm-12">
              
              <h2 class="section-title">Servicios Laborales<span class="item-divider-3"></span></h2>

              <p class="section-text">Dentro del derecho Civil tratamos diferente áreas </p>

              <div class="row">
                <div class="col-sm-6">
                  
                  <ul class="custom-list type-4">

                    <li><a href="#">Despisos</a></li>
                    <li><a href="#">Sanciones</a></li>
                    <li><a href="#">Tramitación y revisión de incapacidades</a></li>

                  </ul>

                </div>
                <div class="col-sm-6">
                  
                  <ul class="custom-list type-4">

                    <li><a href="#">Accidentes laborales</a></li>
                    <li><a href="#">Acoso laboral</a></li>
                    <li><a href="#">Jubilaciones</a></li>

                  </ul>

                </div>
              </div>

            </div>
          </div>
        </div>

      </div>
      
      <!-- Page section -->
      <div class="page-section-bg2 parallax-section" data-bg="<?= base_url() ?>theme/images/1920x1000_bg3.jpg">

        <div class="container">

          <div class="row">
            <div class="col-sm-4">
              
              <h2 class="section-title">Tienes un caso<br> o consulta?<span class="item-divider-3 style-3"></span></h2>

              <p>Rellena este formulario <br> Nos pondremos en contacto contigo</p>

            </div>
            <div class="col-sm-8">
              
              [contactForm]

            </div>
          </div>

        </div>

      </div>
      
      <!-- Page section -->
      <div class="page-section type-2">
        
        <div class="carousel-type-2">
          
          <div class="owl-carousel brand-box" data-item-margin="0" data-max-items="5" >
            
            <!-- owl item -->
            <div class="brand-item">
              
              <a href="#"><img src="<?= base_url() ?>theme/images/brand1.jpg" alt=""></a>

            </div>

            <!-- owl item -->
            <div class="brand-item">
              
              <a href="#"><img src="<?= base_url() ?>theme/images/brand2.jpg" alt=""></a>

            </div>

            <!-- owl item -->
            <div class="brand-item">
              
              <a href="#"><img src="<?= base_url() ?>theme/images/brand3.jpg" alt=""></a>

            </div>

            <!-- owl item -->
            <div class="brand-item">
              
              <a href="#"><img src="<?= base_url() ?>theme/images/brand4.jpg" alt=""></a>

            </div>

            <!-- owl item -->
            <div class="brand-item">
              
              <a href="#"><img src="<?= base_url() ?>theme/images/brand5.jpg" alt=""></a>

            </div>

          </div>

        </div>

      </div>

    </div>
    [footer]