<div id="wrapper" class="wrapper-container">
  <!-- Chat button -->
  
  [header2]
  <!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->
  
  <!-- - - - - - - - - - - - - - Revolution Slider - - - - - - - - - - - - - - - - -->
  <div class="rev-slider-wrapper full-scr">
    <div id="rev-slider" class="rev-slider tp-simpleresponsive"  data-version="5.0">
      <ul>
        <li data-transition="fade">
          <img src="<?= base_url() ?>theme/images/bg1.jpg" class="rev-slidebg" alt="">
          <!-- - - - - - - - - - - - - - Layer 1 - - - - - - - - - - - - - - - - -->
          <div class="tp-caption tp-resizeme scaption-dark-large"
            data-x="['left','left','left','left']" data-hoffset="10"
            data-y="['middle','middle','middle','middle']" data-voffset="0"
            data-whitespace="nowrap"
            data-frames='[{"delay":150,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
            data-responsive_offset="on"
            data-elementdelay="0.05" >Más de 25 años <br> de experiencia <br> nos avalan <br>
          </div>
          <!-- - - - - - - - - - - - - - End of Layer 1 - - - - - - - - - - - - - - - - -->
          <!-- - - - - - - - - - - - - - Layer 2 - - - - - - - - - - - - - - - - -->
          <div class="tp-caption tp-resizeme"
            data-x="['left','left','left','left']" data-hoffset="10"
            data-y="['middle','middle','middle','middle']" data-voffset="130"
            data-whitespace="nowrap"
            data-frames='[{"delay":450,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
            data-responsive_offset="on"
            data-elementdelay="0.05" ><a href="#contacte" class="btn btn-big btn-style-5 shadow">Pídenos una visita gratuita</a>
          </div>
          <!-- - - - - - - - - - - - - - End of Layer 2 - - - - - - - - - - - - - - - - -->
        </li>
        <li data-transition="fade">
          <img src="<?= base_url() ?>theme/images/bg2.jpg" class="rev-slidebg" alt="">
          <!-- - - - - - - - - - - - - - Layer 1 - - - - - - - - - - - - - - - - -->
          <div class="tp-caption tp-resizeme scaption-dark-large"
            data-x="['left','left','left','left']" data-hoffset="10"
            data-y="['middle','middle','middle','middle']" data-voffset="0"
            data-whitespace="nowrap"
            data-frames='[{"delay":150,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
            data-responsive_offset="on"
            data-elementdelay="0.05" >¿Tienes algún problema jurídico? <br/> NOSOTROS TE AYUDAMOS!
          </div>
          <!-- - - - - - - - - - - - - - End of Layer 1 - - - - - - - - - - - - - - - - -->
          <!-- - - - - - - - - - - - - - Layer 2 - - - - - - - - - - - - - - - - -->
          <!--<div class="tp-caption tp-resizeme"
            data-x="['left','left','left','left']" data-hoffset="10"
            data-y="['middle','middle','middle','middle']" data-voffset="130"
            data-whitespace="nowrap"
            data-frames='[{"delay":450,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
            data-responsive_offset="on"
            data-elementdelay="0.05" >
          </div>-->
          <!-- - - - - - - - - - - - - - End of Layer 2 - - - - - - - - - - - - - - - - -->
          <!-- - - - - - - - - - - - - - Layer 2 - - - - - - - - - - - - - - - - -->
          <div class="tp-caption tp-resizeme"
            data-x="['left','left','left','left']" data-hoffset="10"
            data-y="['middle','middle','middle','middle']" data-voffset="130"
            data-whitespace="nowrap"
            data-frames='[{"delay":450,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
            data-responsive_offset="on"
            data-elementdelay="0.05" ><a href="#contacte" class="btn btn-big btn-style-5 shadow2">Pídenos una visita gratuita</a>
          </div>
          <!-- - - - - - - - - - - - - - End of Layer 2 - - - - - - - - - - - - - - - - -->
        </li>
        <li data-transition="fade">
          <img src="<?= base_url() ?>theme/images/1773x1148_bg3.jpg" class="rev-slidebg" alt="">
          <!-- - - - - - - - - - - - - - Layer 1 - - - - - - - - - - - - - - - - -->
          <div class="tp-caption tp-resizeme scaption-dark-large"
            data-x="['left','left','left','left']" data-hoffset="10"
            data-y="['middle','middle','middle','middle']" data-voffset="0"
            data-whitespace="nowrap"
            data-frames='[{"delay":150,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
            data-responsive_offset="on"
            data-elementdelay="0.05" >Nos dedicamos <br> a tu caso al 100%
          </div>
          <!-- - - - - - - - - - - - - - End of Layer 1 - - - - - - - - - - - - - - - - -->
          <!-- - - - - - - - - - - - - - Layer 2 - - - - - - - - - - - - - - - - -->
          <!--<div class="tp-caption tp-resizeme"
            data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
            data-y="['middle','middle','middle','middle']" data-voffset="100"
            data-whitespace="nowrap"
            data-frames='[{"delay":450,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
            data-responsive_offset="on"
            data-elementdelay="0.05" ><p></p>
          </div>-->
          <!-- - - - - - - - - - - - - - End of Layer 2 - - - - - - - - - - - - - - - - -->
          <!-- - - - - - - - - - - - - - Layer 2 - - - - - - - - - - - - - - - - -->
          <div class="tp-caption tp-resizeme"
            data-x="['left','left','left','left']" data-hoffset="10"
            data-y="['middle','middle','middle','middle']" data-voffset="130"
            data-whitespace="nowrap"
            data-frames='[{"delay":450,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[175%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
            data-responsive_offset="on"
            data-elementdelay="0.05" ><a href="#contacte" class="btn btn-big btn-style-5 shadow3">Pídenos una visita gratuita</a>
          </div>
          <!-- - - - - - - - - - - - - - End of Layer 2 - - - - - - - - - - - - - - - - -->
        </li>
      </ul>
    </div>
    <div class="side-bar flex-row">
      
      <p class="v-text"><b>Experiencia, integridad y dedicación.</b></p>
      <ul class="social-icons v-type style-2">
        <li><a href="#"><i class="icon-facebook"></i></a></li>        
      </ul>
      <span class="v-text"><i class="licon-arrow-left bounce"></i>Bajar</span>
    </div>
  </div>
  <!-- - - - - - - - - - - - - - End of Slider - - - - - - - - - - - - - - - - -->
  <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->
  <div id="content">
    
    <!-- Page section -->
    <!-- 
<div class="page-section">
      
      <div class="container extra">
        
        <div class="row">
          <div class="col-md-6 col-sm-12">
            
            <h2 class="section-title">L'advocat que necessites<br>al teu costat<span class="item-divider-3 style-3"></span></h2>
            <h5 class="sub-title"><b>Tenim l'experiència. Et donem resultats.</b></h5>
            <p>Nosaltres et truquem.
                Deixa’ns el teu nom i telèfon i ens posarem en contacte amb tu. 
                 </p>
            <a href="#" class="btn">Més sobre nosaltres</a>
          </div>
          <div class="col-md-6 col-sm-12">
            
            <div class="team-holder">
              
              <div class="team-item">
                <div class="team-member">
                  <a href="#" class="member-photo">
                    <img src="<?= base_url() ?>theme/images/608x420_img1.jpg" alt="">
                  </a>
                  <div class="team-desc overlay style-2">
                    <div class="member-info">
                      <div class="flex-row flex-justify flex-center">
                        <div class="text-size-medium">                          
                          <form id="contact-form" class="contact-form"> 
                            <div class="flex-row">
                              <div class="col-sm-12 titulovisita">PRIMERA VISITA GRATUÏTA</div>
                            </div>
                            <div class="flex-row">                              
                              <div class="col-sm-6">                                
                                <input type="text" name="cf-name" placeholder="Nom i Cognoms" required>
                              </div>
                              <div class="col-sm-6">                                
                                <input type="email" name="cf-email" placeholder="Email *" required>
                              </div>
                              <div class="col-sm-6">                                
                                <input type="text" name="cf-email" placeholder="Telèfon *" required>
                              </div>
                              <div class="col-sm-6">
                                <button type="submit" class="btn btn-style-2 fuente" data-type="submit">Vull que em truqueu</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
 -->
    
    <!-- Page section -->
    <div class="page-section-bg no-bts">
      
      <div class="content-element5">
        
        <div class="container extra">
          
          <div class="row">
            <div class="col-sm-6">
              
              <div class="pre-title">SERVICIOS</div>
              <h2 class="section-title">Todo lo que podemos hacer por ti<span class="item-divider-3 style-3"></span></h2>
            </div>
            <div class="col-sm-6">
              
              <p>Galan Advocats te acompaña, te orienta en la resolución de tus conflictos y problemáticas legales y te aporta tranquilidad jurídica en tu toma de decisiones.  </p>
            </div>
          </div>
        </div>
      </div>
      
      <!-- Services -->
      <div class="services">
        
        <div class="flex-row">
          
          <!-- Service column -->
          <div class="item-col">
            
            <!-- Service item -->
            <a href="[base_url]serveis/7-desnonaments" class="service-item dark">
              <figure>
                <img src="<?= base_url() ?>theme/images/320x308_img1.jpg" alt="">
              </figure>
              
              <span class="service-title">Desahucios</span>
            </a>
          </div>
          <!-- Service column -->
          <div class="item-col-2">
            
            <!-- Service item -->
            <a href="[base_url]serveis/1-reclamacions-a-assegurances" class="service-item grey">
              <figure>
                <img src="<?= base_url() ?>theme/images/640x308_img1.jpg" alt="">
              </figure>
              
              <span class="service-title">Reclamaciones a seguros </span>
            </a>
          </div>
          <!-- Service column -->
          <div class="item-col">
            
            <!-- Service item -->
            <a href="[base_url]serveis/9-separacions-i-divorcis" class="service-item green">
              <figure>
                <img src="<?= base_url() ?>theme/images/320x308_img2.jpg" alt="">
              </figure>
              
              <span class="service-title">Separaciones y divorcios</span>
            </a>
          </div>
          <!-- Service column -->
          <div class="item-col-2">
            
            <!-- Service item -->
            <a href="[base_url]serveis/22-concurs-de-creditors" class="service-item grey">
              <figure>
                <img src="<?= base_url() ?>theme/images/640x308_img2.jpg" alt="">
              </figure>
              
              <span class="service-title">Concurso de acreedores</span>
            </a>
          </div>
          <!-- Service column -->
          <div class="item-col-2">
            
            <!-- Service item -->
            <a href="[base_url]serveis/12-incapacitaci-judicial" class="service-item grey">
              
              <figure>
                <img src="<?= base_url() ?>theme/images/640x308_img3.jpg" alt="">
              </figure>
              
              <span class="service-title">Incapacitacion judicial</span>
            </a>
          </div>
          <!-- Service column -->
          <div class="item-col">
            
            <!-- Service item -->
            <a href="[base_url]serveis/23-reformes-estatut-ries" class="service-item green">
              <figure>
                <img src="<?= base_url() ?>theme/images/320x308_img3.jpg" alt="">
              </figure>
              
              <span class="service-title">Reformas estatutarias</span>
            </a>
          </div>
          <!-- Service column -->
          <div class="item-col-2">
            
            <!-- Service item -->
            <a href="[base_url]serveis/13-viol-ncia-dom-stica-i-de-g-nere" class="service-item dark">
              <figure>
                <img src="<?= base_url() ?>theme/images/640x308_img4.jpg" alt="">
              </figure>
              
              <span class="service-title">Violencia doméstica y de género</span>
            </a>
          </div>
          <!-- Service column -->
          <div class="item-col">
            
            <!-- Service item -->
            <a href="[base_url]serveis/4-dret-bancari" class="service-item green">
              <figure>
                <img src="<?= base_url() ?>theme/images/320x308_img4.jpg" alt="">
              </figure>
              
              <span class="service-title">Derecho bancario </span>
            </a>
          </div>
          <!-- Service column -->
          <div class="item-col">
            
            <!-- Service item -->
            <a href="[base_url]serveis/14-acomiadaments" class="service-item dark">
              <figure>
                <img src="<?= base_url() ?>theme/images/320x308_img5.jpg" alt="">
              </figure>
              
              <span class="service-title">Despidos</span>
            </a>
          </div>
          <!-- Service column -->
          <div class="item-col">
            
            <!-- Service item -->
            <a href="[base_url]serveis/25-liquidacions-d-empreses" class="service-item green">
              <figure>
                <img src="<?= base_url() ?>theme/images/320x308_img6.jpg" alt="">
              </figure>
              
              <span class="service-title">Liquidaciones de empresas</span>
            </a>
          </div>
          <!-- Service column -->
          <div class="item-col-2">
            
            <!-- Service item -->
            <a href="[base_url]serveis/8-reclamacions-d-impagats" class="service-item dark">
              <figure>
                <img src="<?= base_url() ?>theme/images/640x308_img5.jpg" alt="">
              </figure>
              
              <span class="service-title">Reclamaciones de impagados </span>
            </a>
          </div>
          <!-- Service column -->
          <div class="item-col">
            
            <!-- Service item -->
            <a href="[base_url]serveis/3-neglig-ncies-m-diques" class="service-item green">
              <figure>
                <img src="<?= base_url() ?>theme/images/320x308_img7.jpg" alt="">
              </figure>
              
              <span class="service-title">Negligencias médicas</span>
            </a>
          </div>
          <!-- Service column -->
          <div class="item-col">
            
            <!-- Service item -->
            <a href="[base_url]serveis/2-accidents-de-tr-nsit" class="service-item dark">
              <figure>
                <img src="<?= base_url() ?>theme/images/320x308_img8.jpg" alt="">
              </figure>
              
              <span class="service-title">Accidentes de tráfico</span>
            </a>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Page section -->
    <div class="page-section">
      
      <div class="container extra">
        
        <div class="content-element5">
          
          <div class="align-center">
            
            <div class="pre-title">NUESTRO EQUIPO</div>
            <h2 class="section-title">Conócenos a todos<span class="item-divider-3 style-3"></span></h2>
            <p>Un gran equipo humano y profesional dispuesto a acompañarte y asesorarte durante todo el proceso jurídico. </p>
          </div>
        </div>
        <div class="carousel-type-2">
          
          <div class="team-holder owl-carousel" data-max-items="3" data-item-margin="30">
            
            <div class="owl-item">
              
              <!-- team element -->
              <div class="team-item">
                <div class="team-member">
                  <a href="#" class="member-photo">
                    <img src="<?= base_url() ?>theme/images/jesus.jpg" alt="">
                  </a>
                  <div class="team-desc overlay">
                    <div class="member-info">
                      <h5 class="member-name"><a href="#">Jesús Galán Galán</a></h5>
                      <h6 class="member-position">Socio fundador</h6>
                      <p>Administrador de Galán Advocats</p>
                      <a href="mailto:jesus.galan@galanadvocats.com" class="btn">Contactar</a>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
            <div class="owl-item">
              
              <!-- team element -->
              <div class="team-item">
                <div class="team-member">
                  <a href="#" class="member-photo">
                    <img src="<?= base_url() ?>theme/images/rosa.jpg" alt="">
                  </a>
                  <div class="team-desc overlay">
                    <div class="member-info">
                      <h5 class="member-name"><a href="#">Rosa A. Llorach Carbonell</a></h5>
                      <h6 class="member-position">Abogada</h6>
                      <p> </p>
                      <a href="mailto:rosa.angels@galanadvocats.com" class="btn">Contactar</a>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
            <div class="owl-item">
              
              <!-- team element -->
              <div class="team-item">
                <div class="team-member">
                  <a href="#" class="member-photo">
                    <img src="<?= base_url() ?>theme/images/merce.jpg" alt="">
                  </a>
                  <div class="team-desc overlay">
                    <div class="member-info">
                      <h5 class="member-name"><a href="#">Mercè March Clarasó</a></h5>
                      <h6 class="member-position">Abogada</h6>
                      <p> </p>
                      <a href="mailto:info@galanadvocats.com" class="btn">Contactar</a>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
            <div class="owl-item">
            
              <!-- team element -->
              <div class="team-item">
                <div class="team-member">
                  <a href="#" class="member-photo">
                    <img src="<?= base_url() ?>theme/images/alba.jpg" alt="">
                  </a>
                  <div class="team-desc overlay">
                    <div class="member-info">
                      <h5 class="member-name"><a href="#">Alba Calvet Pujol</a></h5>
                      <h6 class="member-position">Servicios administrativos y secretaría</h6>
                      <p> </p>
                      <a href="mailto:alba.calvet@galanadvocats.com" class="btn">Contactar</a>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
      <div class="page-section-bg" id="equip">

          <div class="container">

              <div class="content-element5">

                  <div class="align-center">

                      <div class="pre-title">Desde el blog</div>
                      <h2 class="section-title">Últimas Noticias<span class="item-divider-3 style-3"></span></h2>

                  </div>

              </div>

              <div class="entry-box">
                  <div class="flex-row row">
                    [foreach:blog]
                      <div class="col-sm-4 col-xs-6">
                          <!-- entry -->
                          <div class="entry">
                              <!-- - - - - - - - - - - - - - Entry attachment - - - - - - - - - - - - - - - - -->
                              <div class="thumbnail-attachment">
                                  <a href="[link]">
                                    <img src="[foto]" alt="">
                                  </a>
                              </div>
                              <!-- - - - - - - - - - - - - - Entry body - - - - - - - - - - - - - - - - -->
                              <div class="entry-body">
                                  <div class="entry-meta">
                                      <time class="entry-date" datetime="2018-10-24">[fecha]</time>
                                      <a href="<?= base_url('blog') ?>" class="entry-cat">Noticias</a>
                                  </div>
                                  <h5 class="entry-title">
                                    <a href="[link]">[titulo]</a>
                                  </h5>
                              </div>
                          </div>
                      </div>
                      [/foreach]
                  </div>
              </div>

              <div class="align-center">

                  <a href="<?= base_url('blog') ?>" class="btn">Más noticias</a>

              </div>

          </div>

      </div>
    <!-- Page section -->
    <div class="page-section-bg2" id="contacte">
      
      <div class="container extra">
        
        <div class="row">
          <div class="col-md-7 col-sm-12">
            
            <div class="content-element5">
              
              <h2 class="section-title">Pide la primera consulta gratuita<span class="item-divider-3 style-3"></span></h2>
              <p>Obtened vuestra revisión gratuita en 24 horas. Todos los campos obligatorios.</p>
            </div>
            [contactForm]
          </div>
          <div class="col-md-5 col-sm-12">
            
            <div class="push-left-30">
              <h2 class="section-title">Contacta'ns<span class="item-divider-3 style-3"></span></h2>
              
              <div class="content-element5">
                <div class="our-info">
                  
                  <p><i class="licon-map-marker"></i>Passeig Jacint Verdaguer, 110. 1r, 2a. 08700 IGUALADA</p>
                  <p content="telephone=no"><i class="licon-telephone"></i>938 031 869</p>
                  <p content="telephone=no"><i class="licon-printer"></i>938 066 062</p>
                  <p><i class="licon-clock3"></i>Lunes - Jueves 9h a 13h y de 16h a 20h / Viernes 9h a 13h </p>

                </div>

                <a href="https://www.google.com/maps/dir//2032+S+Elliott+Ave,+Aurora,+MO+65605/@36.9487043,-93.7878472,12z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x87cf4b1a194c90e1:0xba30bfe0c0a857c!2m2!1d-93.7178072!2d36.9487249" class="info-btn">Troba l'adreça</a>
              </div>
              
              <ul class="social-icons">
                
                <li><a href="#"><i class="icon-facebook"></i></a></li>
              <!--  <li><a href="#"><i class="icon-twitter"></i></a></li>
                <li><a href="#"><i class="icon-gplus-3"></i></a></li>
                <li><a href="#"><i class="icon-linkedin-3"></i></a></li>-->
                
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
  <!-- Google map -->
  <div class="map-section">
    <div id="googleMap" class="map-container"></div>
  </div>
  [footer]
</div>