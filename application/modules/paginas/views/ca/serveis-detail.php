<!-- Chat button -->
<!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->
[header]
<div class="breadcrumbs-wrap style-2">
  <div class="container">
    
    <h1 class="page-title">[titulo]</h1>
    <ul class="breadcrumbs">
      <li><a href="<?= site_url() ?>">Inici</a></li>
      <li>Serveis</li>
    </ul>
  </div>
</div>
<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->
<!-- WELCOME -->
<div id="content" class="page-content-wrap2">
  
  <div class="container">
    
    <div class="row">
      
      <main id="main" class="col-md-9 col-sm-12">
        
        <div class="content-element2">
          
          <div class="content-element5">
            
            <div class="row">
              <div class="col-xs-6">
                
                <img src="[foto]" alt="">
              </div>
              <div class="col-xs-6">
                
                <div class="content-element3">
                  <div class="our-info style-2">
                    
                    <p><i class="licon-briefcase"></i>[nombre]</p>
                    <p content="telephone=no"><i class="licon-telephone"></i> [telefono]</p>
                    <p><i class="licon-at-sign"></i><a href="mailto:[email]" class="custom-link">[email]</a></p>
                    
                  </div>
                  
                </div>
                <ul class="social-icons style-3">
                  
                  <li><a href="[facebook]"><i class="icon-facebook"></i></a></li>
                  
                </ul>
              </div>
            </div>
          </div>
          <div class="content-element5">
            [texto]
          </div>
        </div>
      </main>
      <aside id="sidebar" class="sidebar col-md-3 col-sm-12">
        
        <!-- Widget -->
        <div class="widget with-bg">
          
          <h5 class="widget-title">Altres Serveis</h5>
          <ul class="minus-list">
            [foreach:relacionados]
            <li><a href="[link_rel]">[titulo_rel]</a></li>
            [/foreach]
          </ul>
        </div>
        <!-- Widget -->
        <div class="widget with-bg2">
          <h5 class="widget-title">Contacta amb nosaltres</h5>
          <p class="text-size-small">Emplena aquest formulari.
          Contactarem amb tu al més aviat possible.</p>
          <form id="contact-form" class="contact-form" action="<?= base_url('paginas/frontend/contacto') ?>">
            <div class="flex-row">
              <div class="col-sm-12">
                <input type="text" name="nombre" placeholder="Nom i Cognoms *" required>
              </div>
              <div class="col-sm-12">
                <input type="tel" name="telefono" placeholder="Telèfon *" required>
              </div>
              <div class="col-sm-12">
                <input type="email" name="email" placeholder="Email *" required>
              </div>
              <div class="col-sm-12">
                <textarea rows="3" name="message" placeholder="Missatge *" required></textarea>
              </div>
              <div class="col-sm-12">
                <button type="submit" class="btn btn-style-2" data-type="submit">Enviar</button>
              </div>
            </div>
          </form>
        </div>
      </aside>
    </div>
  </div>
</div>
[footer]