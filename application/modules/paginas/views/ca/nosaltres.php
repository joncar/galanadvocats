<!-- Chat button -->

    

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

    [header]

    <!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap">

      <div class="container">
        
        <h1 class="page-title">Nosaltres</h1>

        <ul class="breadcrumbs">

          <li><a href="index.html">Inici</a></li>
          <li>Nosaltres</li>

        </ul>

      </div>

    </div>
<div id="content">

      <!-- Page section -->
      <div class="page-section half-bg-col">

        <div class="img-col-left"><div class="col-bg" data-bg="<?= base_url() ?>theme/images/grupo.jpg"></div></div>
        
        <div class="container">
          <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
              
              <h2 class="section-title">Qui som Galan Advocats<span class="item-divider-3 style-3"></span></h2>

              <p>Galán Advocats és un despatx que ofereix els seus serveis d’assessorament jurídic integral des de l'any 2000, oferint assessorament legal i defensa jurídica a particulars i empreses. La satisfacció dels nostres clients és la millor prova de la feina ben feta.
  </p>
              <p>Galan Advocats és la garantia que el seu cas està en bones mans. La nostra professionalitat, rigor, dedicació i proximitat faran que, des del primer moment, vostè prengui consciència que els seus interessos seran ben defensats.
</p>

              <a href="[base_url]#equip" class="btn">Coneix el nostre equip</a>

            </div>
          </div>
        </div>

      </div>

      <!-- Page section -->
      <div class="page-section-bg2 half-bg-col">

        <div class="img-col-right"><div class="col-bg" data-bg="<?= base_url() ?>theme/images/960x644_img2.jpg"></div></div>
        
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              
              <h2 class="section-title">Els nostres Valors<span class="item-divider-3 style-3"></span></h2>

              <div class="icons-box style-2">
              
                <div class="row flex-row">
              
                  <div class="col-sm-12">
                    
                    <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->        
                    <div class="icons-wrap">
              
                      <div class="icons-item">
                        <div class="item-box">
                          <i class="licon-balance"></i>
                          <h5 class="icons-box-title"><a href="#">Experiència</a></h5>
                          <p>Els anys de treball avalen la nostra experiència, els nostres resultats ho confirmen. </p>
                        </div>
                      </div>
              
                    </div>
              
                  </div>
                  <div class="col-sm-12">
                    
                    <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->        
                    <div class="icons-wrap">
              
                      <div class="icons-item">
                        <div class="item-box">
                          <i class="licon-man"></i>
                          <h5 class="icons-box-title"><a href="#">Integritat</a></h5>
                          <p>Honradesa, honestedat i responsabilitat, la principal raó per confiar en nosaltres.</p>
                        </div>
                      </div>
              
                    </div>
              
                  </div>
                  <div class="col-sm-12">
                    
                    <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->        
                    <div class="icons-wrap">
              
                      <div class="icons-item">
                        <div class="item-box">
                          <i class="licon-briefcase"></i>
                          <h5 class="icons-box-title"><a href="#">Dedicació</a></h5>
                          <p>Treballem per tu, directament i en constant comunicació.</p>
                        </div>
                      </div>
              
                    </div>
              
                  </div>
                    
                </div>
              
              </div>

            </div>
            <div class="col-md-6"></div>
          </div>
        </div>

      </div>

      <!-- Page section -->
      <!-- 
<div class="page-section type-2">
        
        <div class="container">

          <div class="row counter-wrap">

            <div class="col-lg-3 col-md-3 col-xs-6">

              <div class="counter">
                <div class="count-item">
                  <span class="licon-hammer2"></span>
                  <h3 class="timer count-number" data-to="50" data-speed="1500">0</h3>
                </div>
                <p>Anys d'experiència</p>
              </div>

            </div>
            <div class="col-lg-3 col-md-3 col-xs-6">

              <div class="counter">
                <div class="count-item">
                  <span class="licon-happy"></span>
                  <h3 class="timer count-number" data-to="389" data-speed="1500">0</h3>
                </div>
                <p>Clients satisfets</p>
              </div>

            </div>
            <div class="col-lg-3 col-md-3 col-xs-6">

              <div class="counter">
                <div class="count-item">
                  <span class="licon-group-work"></span>
                  <h3 class="timer count-number" data-to="15" data-speed="1500">0</h3>
                </div>
                <p>El nostre equip</p>
              </div>

            </div>
            <div class="col-lg-3 col-md-3 col-xs-6">

              <div class="counter">
                <div class="count-item">
                  <span class="licon-trophy2"></span>
                  <h3 class="timer count-number" data-to="227" data-speed="1500">0</h3>
                </div>
                <p>Casos resolts positivament</p>
              </div>

            </div>

          </div>

        </div>

      </div>
 -->

      <!-- Page section -->
      <div class="page-section half-bg-col two-cols">

        <div class="img-col-left"><div class="col-bg" data-bg="<?= base_url() ?>theme/images/960x616_bg1.jpg"></div></div>

        <div class="img-col-right"><div class="col-bg" data-bg="<?= base_url() ?>theme/images/960x560_bg3.jpg"></div></div>
        
        <div class="container extra">
          <div class="row">
            <div class="col-md-6 col-sm-12">
              
              <h2 class="section-title">Serveis Civils<span class="item-divider-3 style-3"></span></h2>

              <p class="section-text">Dins el dret Civil tractem diferent àrees <br>  <br>  </p>

              <div class="row">
                <div class="col-sm-6">
                  
                  <ul class="custom-list type-1">

                    <li><a href="#">Reclamacions a assegurances</a></li>
                    <li><a href="#">Accidents de trànsit</a></li>
                    <li><a href="#">Negligències mèdiques</a></li>

                  </ul>

                </div>
                <div class="col-sm-6">
                  
                  <ul class="custom-list type-1">

                    <li><a href="#">Dret bancari</a></li>
                    <li><a href="#">Immobiliària i construcció</a></li>
                    <li><a href="#">Arrendaments</a></li>

                  </ul>

                </div>
              </div>

            </div>
            <div class="page-section-bg2 green col-md-6 col-sm-12">
              
              <h2 class="section-title">Serveis Laborals<span class="item-divider-3"></span></h2>

              <p class="section-text">Dins el dret Civil tractem diferent àrees </p>

              <div class="row">
                <div class="col-sm-6">
                  
                  <ul class="custom-list type-4">

                    <li><a href="#">Acomiadaments</a></li>
                    <li><a href="#">Sancions</a></li>
                    <li><a href="#">Tramitació i revisió d'incapacitats</a></li>

                  </ul>

                </div>
                <div class="col-sm-6">
                  
                  <ul class="custom-list type-4">

                    <li><a href="#">Accidents laborals</a></li>
                    <li><a href="#">Assetjament laboral</a></li>
                    <li><a href="#">Jubilacions</a></li>

                  </ul>

                </div>
              </div>

            </div>
          </div>
        </div>

      </div>
      
      <!-- Page section -->
      <div class="page-section-bg2 parallax-section" data-bg="<?= base_url() ?>theme/images/1920x1000_bg3.jpg">

        <div class="container">

          <div class="row">
            <div class="col-sm-4">
              
              <h2 class="section-title">Tens un cas<br> o consulta?<span class="item-divider-3 style-3"></span></h2>

              <p>Emplena aquest formulari <br> Ens posarem en contacte amb tu</p>

            </div>
            <div class="col-sm-8">
              
              [contactForm]

            </div>
          </div>

        </div>

      </div>
      
      <!-- Page section -->
      <div class="page-section type-2">
        
        <div class="carousel-type-2">
          
          <div class="owl-carousel brand-box" data-item-margin="0" data-max-items="5" >
            
            <!-- owl item -->
            <div class="brand-item">
              
              <a href="#"><img src="<?= base_url() ?>theme/images/brand1.jpg" alt=""></a>

            </div>

            <!-- owl item -->
            <div class="brand-item">
              
              <a href="#"><img src="<?= base_url() ?>theme/images/brand2.jpg" alt=""></a>

            </div>

            <!-- owl item -->
            <div class="brand-item">
              
              <a href="#"><img src="<?= base_url() ?>theme/images/brand3.jpg" alt=""></a>

            </div>

            <!-- owl item -->
            <div class="brand-item">
              
              <a href="#"><img src="<?= base_url() ?>theme/images/brand4.jpg" alt=""></a>

            </div>

            <!-- owl item -->
            <div class="brand-item">
              
              <a href="#"><img src="<?= base_url() ?>theme/images/brand5.jpg" alt=""></a>

            </div>

          </div>

        </div>

      </div>

    </div>
    [footer]