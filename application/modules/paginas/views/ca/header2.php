<!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

    <nav id="mobile-advanced" class="mobile-advanced"></nav>

    <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

    <header id="header" class="header fixed-header sticky-header">

      <!-- searchform -->

      <div class="searchform-wrap">
        <div class="vc-child h-inherit">

          <form action="<?= base_url('blog') ?>" method="get">
            <button type="submit" class="search-button"></button>
            <div class="wrapper">
              <input type="text" name="direccion" placeholder="Buscar">
            </div>
          </form>

          <button class="close-search-form"></button>

        </div>
      </div>

      <div class="flex-row flex-center flex-justify">

        <div class="top-header flex-row flex-center">
          
          <!-- logo -->
        
          <div class="logo-wrap">
          
            <a href="<?= base_url() ?>" class="logo"><img src="<?= base_url() ?>theme/images/logo.jpg" alt=""></a>
          
          </div>
          
          <!-- - - - - - - - - - - - / Mobile Menu - - - - - - - - - - - - - -->
          
          <!--main menu-->
          
          <div class="menu-holder flex-row flex-justify">
            
            <div class="menu-wrap">
          
              <div class="nav-item flex-row flex-justify flex-center">
                
                <!-- - - - - - - - - - - - - - Navigation - - - - - - - - - - - - - - - - -->
        
                <nav id="main-navigation" class="main-navigation">
                  <?php $this->load->view('includes/template/menu'); ?>
                </nav>
        
                <!-- - - - - - - - - - - - - end Navigation - - - - - - - - - - - - - - - -->
        
                <div class="search-holder">
                
                  <button type="button" class="search-button"></button>
        
                </div>
        
              </div>
          
            </div>

          </div>

        </div>

        <!-- Contact info -->

        <ul class="contact-info">
          <li class="info-item">
            <i class="licon-telephone"></i>
            <div class="item-info">
              <span content="telephone=no">938 031 869</span>
            </div>
          </li>
          <li class="info-item" style=" padding-right: 4px; padding-top: 5px;">            
            <div class="item-info">              
              <a href="<?= base_url('main/traduccion/es') ?>" style="color:white; <?= $_SESSION['lang']=='ca'?'text-decoration: underline;':''; ?>"><span>ESP</span></a> 
            </div>
          </li>
          <li class="info-item" style=" padding-left: 4px; padding-top: 5px;">            
            <div class="item-info">              
              <a href="<?= base_url('main/traduccion/ca') ?>" style="color:white; <?= $_SESSION['lang']=='es'?'text-decoration: underline;':''; ?>"><span>CAT</span></a>
            </div>
          </li>
        </ul>

      </div>
      
    </header>
