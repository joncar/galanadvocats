<!-- Chat button -->

<!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->
[header]
<div class="breadcrumbs-wrap">
  <div class="container">
    
    <h1 class="page-title">Contacte</h1>
    <ul class="breadcrumbs">
      <li><a href="<?= base_url() ?>">Inici</a></li>
      <li>Contacte</li>
    </ul>
  </div>
</div>
<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->
<!-- WELCOME -->
  <div id="content">
    
    <div class="page-section type-3">
      
      <div class="container">
        
        <ul class="contact-info style-3 flex-row fx-col-3">
          <li class="info-item">
            <i class="licon-telephone"></i>
            <div class="item-info">
              <span content="telephone=no">93 803 18 69</span>
            </div>
          </li>
          <li class="info-item">
            <i class="licon-at-sign"></i>
            <div class="item-info">
              <a href="#">info@galanadvocats.com</a>
            </div>
          </li>
          <li class="info-item">
            <i class="licon-share2"></i>
            <div class="item-info">
              <ul class="social-icons">
                <li><a href="#"><i class="icon-facebook"></i></a></li>
                <li><a href="#"><i class="icon-twitter"></i></a></li>
                <li><a href="#"><i class="icon-gplus-3"></i></a></li>
                <li><a href="#"><i class="icon-linkedin-3"></i></a></li>
              </ul>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <div class="page-section page-section-bg half-bg-col">
      <div class="img-col-right">
        <div class="col-bg">
          <!-- Google map -->
          <div class="map-section">
            <div id="googleMap" class="map-container"></div>
          </div>
        </div>
      </div>
      
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            
            <h2 class="section-title">On som?<span class="item-divider-3 style-3"></span>
            </h2>
            
            <div class="content-element5">
              <div class="our-info">
                
                <p><i class="licon-map-marker"></i>Passeig Jacint Verdaguer, 110. 1r, 2a. Igualada </p>
                <p content="telephone=no"><i class="licon-telephone"></i>93 803 18 69</p>
                <p content="telephone=no"><i class="licon-printer"></i>93 806 60 62</p>
                <p><i class="licon-clock3"></i>Dilluns - Dijous 9h a 13h i de 16h a 20h / Divendres 9h a 13h </p>
                
              </div>
              
              <a href="https://www.google.com/maps/search/Passeig+Jacint+Verdaguer,+110.+1r,+2a.+Igualada+/@41.580365,1.6200909,17z/data=!3m1!4b1" class="info-btn">Veure Google Maps</a>
            </div>
          </div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
    <div class="page-section page-section-bg2 parallax-section" data-bg="images/1920x1000_bg3.jpg">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            
            <h2 class="section-title">Et podem ajudar?<span class="item-divider-3 style-3"></span>
            </h2>
            <p>Emplena aquest formulari i contactarem amb tu al més aviat possible.</p>
          </div>
          <div class="col-sm-8">
            [response]
            [contactForm]
          </div>
        </div>
      </div>
    </div>
  </div>