<!-- Chat button -->

    

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

    [header]

    <div class="breadcrumbs-wrap style-3" data-bg="<?= base_url() ?>theme/images/1920x225_bg1.jpg">

      <div class="container">
        
        <h1 class="page-title">Blog</h1>

        <ul class="breadcrumbs">

          <li><a href="<?= base_url() ?>">Inici</a></li>
          <li>Blog</li>

        </ul>

      </div>

    </div>

    <!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->
<!-- WELCOME -->
    <div id="content" class="page-content-wrap2">
      
      <div class="container">
        
        <div class="row">
          
          <main id="main" class="col-md-8 col-sm-12">
            
            <!-- Entries -->
            <div class="entry-box with-sep">
              
              
              [foreach:blog]
              <!-- entry -->
              <div class="entry">
              
                <!-- - - - - - - - - - - - - - Entry attachment - - - - - - - - - - - - - - - - -->
                <div class="thumbnail-attachment carousel-type-2">
                  <div class="owl-carousel var2 style-2" data-max-items="1">
                    
                    <img src="[foto]" alt="">

                    <img src="<?= base_url() ?>theme/<?= base_url() ?>theme/images/750x504_img3.jpg" alt="">

                    <img src="<?= base_url() ?>theme/<?= base_url() ?>theme/images/750x504_img4.jpg" alt="">

                  </div>
                </div>
                
                
                  <!-- - - - - - - - - - - - - - Entry body - - - - - - - - - - - - - - - - -->
                  <div class="entry-body">
                    
                    <div class="label">
                      <div class="date">
                        <h6 class="month">[mes]</h6>
                        <h4 class="day">[dia]</h4>                        
                      </div>
                      <span class="icon licon-news"></span>
                    </div>
                    <div class="wrapper">
                      
                      <div class="entry-meta">
    
                        by <a href="[link]" class="entry-cat">[user]</a>
                        <a href="[link]" class="entry-cat">events</a>
                        <a href="[link]" class="entry-cat">[comentarios] comentaris</a>

                      </div>
                      <h5 class="entry-title"><a href="[link]">[titulo]</a></h5>
                      <p>[texto]</p>
                      <div class="flex-row flex-justify tooltips">
                        
                        <a href="[link]" class="info-btn with-icon">Veure més <i class="licon-arrow-right"></i></a>
                        <div class="info-btn share with-icon left-tooltip">
                          <div class="tooltip">
                            <ul class="social-icons style-2">

                              <li><a href="#"><i class="icon-facebook"></i></a></li>
                              <li><a href="#"><i class="icon-twitter"></i></a></li>
                              <li><a href="#"><i class="icon-gplus-3"></i></a></li>
                              <li><a href="#"><i class="icon-linkedin-3"></i></a></li>

                            </ul>
                          </div>
                          <i class="licon-share2"></i> Compartir</div>

                      </div>

                    </div>

                  </div>

                </div>
              [/foreach]


            </div>
            
            <!-- Pagination -->
            <ul class="pagination">
              [paginacion]
            </ul>

          </main>
          [aside]

        </div>

      </div>

    </div>
    [footer]