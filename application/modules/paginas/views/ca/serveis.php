<!-- Chat button -->

    

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

    [header]

    <div class="breadcrumbs-wrap style-3 align-center" data-bg="<?= base_url() ?>theme/images/1920x225_bg1.jpg">

      <div class="container">
        
        <h1 class="page-title">Serveis</h1>

        <ul class="breadcrumbs">

          <li><a href="index.html">Home</a></li>
          <li>Attorneys List v1</li>

        </ul>

      </div>

    </div>

    <!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->
            <!-- WELCOME --><!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

<div id="content" class="page-content-wrap2">
      
      <div class="services">

        <!-- - - - - - - - - - - - - - Filter - - - - - - - - - - - - - - - - -->     

        <div id="options">
          <div id="filters" class="isotope-nav">
            <button class="is-checked" data-filter="*">Tots</button>
            <button data-filter=".category_2">Personal Injury</button>
            <button data-filter=".category_3">Estate Planning</button>
            <button data-filter=".category_4">Criminal Defense</button>
            <button data-filter=".category_5">Insurance Claims</button>
            <button data-filter=".category_6">Business & Real Estate</button>
          </div>
        </div>

        <!-- - - - - - - - - - - - - - End of Filter - - - - - - - - - - - - - - - - -->

        <div class="isotope four-collumn clearfix team-holder style-2" data-isotope-options='{"itemSelector" : ".item","layoutMode" : "fitRows","transitionDuration":"0.7s","fitRows" : {"columnWidth":".item"}}'>
          
          <!-- Isotope column -->

          <div class="item category_2">
            
            <!-- Team item -->

            <div class="team-item">

              <div class="team-member">
                <a href="#" class="member-photo">
                  <img src="<?= base_url() ?>theme/images/480x532_photo1.jpg" alt="">
                </a>
                <div class="team-desc overlay">
                  <div class="member-info">
                    <h5 class="member-name"><a href="#">Sam Hicks</a></h5>
                    <h6 class="member-position">Personal Injury</h6>
                    <a href="#" class="btn">View Profile</a>
                  </div>
                </div>
              </div>

            </div>

          </div>

          <!-- Isotope column -->

          <div class="item category_3">
            
            <!-- Team item -->

            <div class="team-item">

              <div class="team-member">
                <a href="#" class="member-photo">
                  <img src="<?= base_url() ?>theme/images/480x532_photo2.jpg" alt="">
                </a>
                <div class="team-desc overlay">
                  <div class="member-info">
                    <h5 class="member-name"><a href="#">Bradley Hynson</a></h5>
                    <h6 class="member-position">Estate Planning</h6>
                    <a href="#" class="btn">View Profile</a>
                  </div>
                </div>
              </div>

            </div>

          </div>

          <!-- Isotope column -->

          <div class="item category_4">
            
            <!-- Team item -->

            <div class="team-item">

              <div class="team-member">
                <a href="#" class="member-photo">
                  <img src="<?= base_url() ?>theme/images/480x532_photo3.jpg" alt="">
                </a>
                <div class="team-desc overlay">
                  <div class="member-info">
                    <h5 class="member-name"><a href="#">Jessica Priston</a></h5>
                    <h6 class="member-position">Criminal Defense</h6>
                    <a href="#" class="btn">View Profile</a>
                  </div>
                </div>
              </div>

            </div>

          </div>

          <!-- Isotope column -->

          <div class="item category_6">
            
            <!-- Team item -->

            <div class="team-item">

              <div class="team-member">
                <a href="#" class="member-photo">
                  <img src="<?= base_url() ?>theme/images/480x532_photo4.jpg" alt="">
                </a>
                <div class="team-desc overlay">
                  <div class="member-info">
                    <h5 class="member-name"><a href="#">Marta Healy</a></h5>
                    <h6 class="member-position">Business &amp; Real Estate</h6>
                    <a href="#" class="btn">View Profile</a>
                  </div>
                </div>
              </div>

            </div>

          </div>

          <!-- Isotope column -->

          <div class="item category_5">
            
            <!-- Team item -->

            <div class="team-item">

              <div class="team-member">
                <a href="#" class="member-photo">
                  <img src="<?= base_url() ?>theme/images/480x532_photo1.jpg" alt="">
                </a>
                <div class="team-desc overlay">
                  <div class="member-info">
                    <h5 class="member-name"><a href="#">John McCoist</a></h5>
                    <h6 class="member-position">Insurance Claims</h6>
                    <a href="#" class="btn">View Profile</a>
                  </div>
                </div>
              </div>

            </div>

          </div>

          <!-- Isotope column -->

          <div class="item category_4">
            
            <!-- Team item -->

            <div class="team-item">

              <div class="team-member">
                <a href="#" class="member-photo">
                  <img src="<?= base_url() ?>theme/images/480x532_photo2.jpg" alt="">
                </a>
                <div class="team-desc overlay">
                  <div class="member-info">
                    <h5 class="member-name"><a href="#">Alan Smith</a></h5>
                    <h6 class="member-position">Criminal Defense</h6>
                    <a href="#" class="btn">View Profile</a>
                  </div>
                </div>
              </div>

            </div>

          </div>

          <!-- Isotope column -->

          <div class="item category_6">
            
            <!-- Team item -->

            <div class="team-item">

              <div class="team-member">
                <a href="#" class="member-photo">
                  <img src="<?= base_url() ?>theme/images/480x532_photo3.jpg" alt="">
                </a>
                <div class="team-desc overlay">
                  <div class="member-info">
                    <h5 class="member-name"><a href="#">Sandra Vancova</a></h5>
                    <h6 class="member-position">Business & Real Estate</h6>
                    <a href="#" class="btn">View Profile</a>
                  </div>
                </div>
              </div>

            </div>

          </div>

          <!-- Isotope column -->

          <div class="item category_2">
            
            <!-- Team item -->

            <div class="team-item">

              <div class="team-member">
                <a href="#" class="member-photo">
                  <img src="<?= base_url() ?>theme/images/480x532_photo4.jpg" alt="">
                </a>
                <div class="team-desc overlay">
                  <div class="member-info">
                    <h5 class="member-name"><a href="#">Anete Puga</a></h5>
                    <h6 class="member-position">Personal Injury</h6>
                    <a href="#" class="btn">View Profile</a>
                  </div>
                </div>
              </div>

            </div>

          </div>

        </div>      
          
      </div>

    </div>
    [footer]