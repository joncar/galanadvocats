<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Evento extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function eventos_tipos(){
            $crud = $this->crud_function('','');                                    
            $crud->unset_delete();
            $crud->callback_before_delete(function($id){
                get_instance()->db->delete('eventos',array('categoria_eventos_id'=>$id));
            });
            $crud->field_type('idioma','dropdown',array('es'=>'Castellano','ca'=>'Catalán','en'=>'Ingles'));
            $crud->set_clone();
            $crud = $crud->render();            
            $this->loadView($crud);
        }
        
        function eventos($x = '',$y = ''){
            $crud = $this->crud_function('','');                        
            $crud->set_field_upload('foto','img/eventos');
            $crud->set_field_upload('foto_grande','img/eventos');            
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Castellano','en'=>'Ingles'));
            $crud->set_clone();
            //$crud->unset_columns('categoria_eventos_id');
            //$crud->where('categoria_eventos_id',$x);
            //$crud->field_type('categoria_eventos_id','hidden',$x);
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>


