<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        } 


        function index(){
        	$this->loadView(array('view'=>'list','link'=>'serveis','title'=>'Serveis'));
        }

        public function read($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $servicios = new Bdsource();
                $servicios->where('id',$id);
                $servicios->init('servicios',TRUE);
                $this->servicios->link = site_url('serveis/'.toURL($this->servicios->id.'-'.$this->servicios->titulo));
                $this->servicios->foto = base_url('img/servicios/'.$this->servicios->foto);
                
                

                $relacionados = new Bdsource();
                $relacionados->limit = array(4); 
                $relacionados->where('categorias_id',$this->servicios->categorias_id);
                $relacionados->where('id !=',$this->servicios->id);                                
                $relacionados->init('servicios',FALSE,'relacionados');
                foreach($this->relacionados->result() as $n=>$b){
                    $this->relacionados->row($n)->link_rel = site_url('serveis/'.toURL($b->id.'-'.$b->titulo));
                    $this->relacionados->row($n)->titulo_rel = $b->titulo;
                    $this->relacionados->row($n)->foto = base_url('img/servicios/'.$b->foto);
                }
                $this->loadView(
                    array(
                        'view'=>'detail',
                        'detail'=>$this->servicios,
                        'title'=>$this->servicios->titulo,
                        'relacionados'=>$this->relacionados,
                        'link'=>'serveis'
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
        
        
    }
?>
