<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Servicios extends Panel{
        function __construct() {
            parent::__construct();
        } 
        
        function categorias($x = '',$y = ''){
            $crud = $this->crud_function("","");    
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalan','es'=>'Castellano'));                                  
            $crud = $crud->render();
            $this->loadView($crud);
        }    
        function servicios($x = '',$y = ''){
            $crud = $this->crud_function("","");
            $crud->set_field_upload('foto','img/servicios')
                 ->field_type('idioma','dropdown',array('ca'=>'Catalan','es'=>'Castellano'));                                     
            $crud = $crud->render();
            $this->loadView($crud);
        } 
    }
?>
