<!-- Chat button -->

    

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

    [header]

    <div class="breadcrumbs-wrap style-3 align-center" data-bg="<?= base_url() ?>theme/images/1920x225_bg1.jpg">

      <div class="container">
        
        <h1 class="page-title">Serveis</h1>

        <ul class="breadcrumbs">

          <li><a href="<?= site_url() ?>">Inici</a></li>
          <li>Serveis</li>

        </ul>

      </div>

    </div>

    <!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->
            <!-- WELCOME --><!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

<div id="content" class="page-content-wrap2">
      
      <div class="services">

        <!-- - - - - - - - - - - - - - Filter - - - - - - - - - - - - - - - - -->     

        <div id="options">
          <div id="filters" class="isotope-nav">
            <button class="is-checked" data-filter="*">Tots</button>
        	<?php foreach($this->db->get_where('categorias',array('categorias.idioma'=>$_SESSION['lang']))->result() as $c): ?>
            	<button data-filter=".categoria<?= $c->id ?>"><?= $c->nombre ?></button>
        	<?php endforeach ?>            
          </div>
        </div>

        <!-- - - - - - - - - - - - - - End of Filter - - - - - - - - - - - - - - - - -->

        <div class="isotope four-collumn clearfix team-holder style-2" data-isotope-options='{"itemSelector" : ".item","layoutMode" : "fitRows","transitionDuration":"0.7s","fitRows" : {"columnWidth":".item"}}'>
          
          <!-- Isotope column -->
          <?php 
          	  $this->db->select('categorias.nombre as categoria, servicios.*');
          	  $this->db->join('categorias','categorias.id = servicios.categorias_id');
          	  foreach($this->db->get_where('servicios',array('servicios.idioma'=>$_SESSION['lang']))->result() as $c): 
      	  	?>
	          <div class="item categoria<?= $c->categorias_id ?>">
	            
	            <!-- Team item -->

	            <div class="team-item">

	              <div class="team-member">
	                <a href="<?= base_url('serveis/'.toUrl($c->id.'-'.$c->titulo)) ?>" class="member-photo">
	                  <img src="<?= base_url('img/servicios/'.$c->foto) ?>" alt="">
	                </a>
	                <div class="team-desc overlay">
	                  <div class="member-info">
	                    <h5 class="member-name"><a href="<?= base_url('serveis/'.toUrl($c->id.'-'.$c->titulo)) ?>"><?= $c->titulo ?></a></h5>
	                    <h6 class="member-position"><?= $c->categoria ?></h6>
	                    <a href="<?= base_url('serveis/'.toUrl($c->id.'-'.$c->titulo)) ?>" class="btn">Veure Més</a>
	                  </div>
	                </div>
	              </div>

	            </div>

	          </div>
          <?php endforeach ?>
          

        </div>      
          
      </div>

    </div>
    [footer]