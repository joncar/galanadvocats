<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function notificaciones(){
            $crud = $this->crud_function('','');  
            $crud->set_clone();
            $this->loadView($crud->render());
        }
    }
?>
