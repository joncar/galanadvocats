<li><a href="<?= base_url('blog') ?>?page=1" class="prev-page"></a></li>
<?php for($i=1;$i<=$total_pages;$i++): ?>
  <li>
    <a href='<?= base_url('blog') ?>?page=<?= $i ?>'>
      <span class='page-numbers <?= $i==$current_page?'current':'' ?>'><?= $i ?></span>
    </a>
  </li>
<?php endfor ?>
<li><a href="<?= base_url('blog') ?>?page=<?= $total_pages ?>" class="next-page"></a></li>