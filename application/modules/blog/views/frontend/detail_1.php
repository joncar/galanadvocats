<!-- MAIN CONTENT-->
<div class="main-content">
    <section class="page-banner blog-detail" style="background:url(<?= $detail->foto  ?>); background-size:cover; background-repeat:no-repeat; background-position:center center;">
        <div class="patterndestino"></div>
        <div class="container">
            <div class="page-title-wrapper">
                <div class="page-title-content">
                    <ol class="breadcrumb">
                        <li><a href="index.html" class="link home">Home</a></li>
<!--                        <li><a href="blog.html" class="link home">Blog</a></li>-->
                        <li class="active"><a href="#" class="link"><?=  $detail->titulo ?></a></li>
                    </ol>
                    <div class="clearfix"></div>
                    <h2 class="captions">Blog</h2></div>
            </div>
        </div>
    </section>
    <section class="page-main padding-top padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-8 main-left">
                    <div class="item-blog-detail">
                        <div class="blog-post blog-text">
                            <div class="blog-image">
                                <a href="javascript:void(0)" class="link">
                                    <img src="<?= $detail->foto  ?>" alt="car on a road" class="img-responsive">
                                </a>
                            </div>
                            <div class="blog-content margin-bottom70">
                                <div class="row">
                                    <div class="col-xs-1">
                                        <div class="date">
                                            <h1 class="day"><?= date("d",strtotime($detail->fecha)) ?></h1>
                                            <div class="month"><?= date("m", strtotime($detail->fecha)) ?></div>
                                            <div class="year"><?= date("Y", strtotime($detail->fecha)) ?></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-11 blog-text">
                                        <a href="javascript:void(0)" class="heading"> <?= $detail->titulo ?></a>
                                        <h5 class="meta-info">
                                           Publicat per: <span><?= $detail->user ?></span>
                                            <span class="sep">/</span>
                                            <span class="view-count fa-custom"><?= $detail->visitas ?></span>
                                            <span class="comment-count fa-custom"><?= $comentarios->num_rows() ?></span>
                                        </h5>
                                        <div class="blog-descritption">
                                            <?= $detail->texto ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-detail-tag tags-widget margin-bottom">
                                <span class="content-tag">Tags:</span>
                                <div class="content-widget">
                                    <?php foreach(explode(',',$detail->tags) as $e): ?>
                                            <a href="<?= base_url('blog') ?>?direccion=<?= $e ?>" class="tag"><?= $e ?></a>
                                    <?php endforeach ?>                                    
                                </div>
                            </div>
                            
                            <div class="blog-comment">
                                <div class="comment-count blog-comment-title sideline"><?= $comentarios->num_rows() ?> Comentaris</div>
                                <ul class="comment-list list-unstyled">
                                    
                                    <?php foreach($comentarios->result() as $c): ?>
                                            <li class="media parent">
                                                <div class="comment-item">
                                                    <div class="media-left">
                                                        <a class="media-image">
                                                            <img src="<?= base_url() ?>img/avatar/avatar-02.png" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="media-right">
                                                        <div class="pull-left">
                                                            <div class="author"><?= $c->autor ?></div>
                                                        </div>
                                                        <div class="pull-right time">
                                                            <i class="fa fa-clock-o"> </i>
                                                            <span><?= date("d-m-Y",strtotime($c->fecha)) ?></span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="des"><?= $c->texto ?></div>
                                                        <a href="#comment-form" class="btn-crystal btn"><i class="fa fa-reply"> </i>Respondre</a>
                                                    </div>
                                                </div>                                        
                                            </li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                            <div class="leave-comment">                                
                                <?= @$_SESSION['mensaje']; ?>
                                <?php @$_SESSION['mensaje'] = ""; ?>
                                <div class="blog-comment-title sideline">Deixa el teu comentari</div>
                                <form id="comment-form" method="post" class="contact-form" action="<?= base_url("blog/frontend/comentarios") ?>">
                                    <input type="text" name="autor" placeholder="El teu nom" class="form-control form-input">
                                    <input type="email" name="email" placeholder="El teu Email" class="form-control form-input">
                                    <input type="hidden" name="blog_id" value="<?= $detail->id ?>">
                                    <textarea name="texto" placeholder="Missatge" class="form-control form-input"></textarea>
                                    <div class="contact-submit"></div>
                                    <button type="submit" data-hover="SEND NOW" class="btn btn-slide">
                                        <span class="text">Enviar missatge</span>
                                        <span class="icons fa fa-long-arrow-right">  </span>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 sidebar-widget">
                    <div class="col-2">
                        <div class="search-widget widget">
                            <form action="<?= base_url("blog") ?>">
                                <div class="input-group search-wrapper">
                                    <input type="text" placeholder="Buscar..." name="direccion" class="search-input form-control">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn submit-btn">
                                            <span class="fa fa-search"></span>
                                        </button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="col-1">
                            <div class="recent-post-widget widget">
                                <div class="title-widget">
                                    <div class="title">NOTÍCIES RELACIONADES</div>
                                </div>
                                <div class="content-widget">
                                    <div class="recent-post-list">
                                        
                                    <?php foreach($relacionados->result() as $d): ?>
                                        <div class="single-widget-item">
                                            <div class="single-recent-post-widget">
                                                <a href="<?= $d->link ?>" class="thumb img-wrapper">
                                                    <img src="<?= $d->foto ?>" alt="recent post picture 1">
                                                </a>
                                                <div class="post-info">
                                                    <div class="meta-info">
                                                        <span><?= date("d-m-Y",strtotime($d->fecha)) ?></span>
                                                        <span class="sep">/</span>
                                                        <span class="fa-custom view-count"><?= $d->visitas ?></span>
                                                        <span class="fa-custom comment-count"><?= $d->comentarios ?></span>
                                                    </div>
                                                    <div class="single-rp-preview"><?= $d->titulo ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach ?>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="categories-widget widget">
                                <div class="title-widget">
                                    <div class="title">CATEGORIES</div>
                                </div>
                                <div class="content-widget">
                                    <ul class="widget-list">
                                        <?php foreach($categorias->result() as $d): ?>
                                            <li class="single-widget-item">
                                                <a href="<?= base_url('blog') ?>?blog_categorias_id=<?= $d->id ?>" class="link">
                                                    <span class="fa-custom category"><?= $d->blog_categorias_nombre ?></span>
                                                    <span class="count"><?= $d->cantidad ?></span>
                                                </a>
                                            </li>
                                        <?php endforeach ?>    
                                    </ul>
                                </div>
                            </div>
                            <div class="tags-widget widget">
                                <div class="title-widget">
                                    <div class="title">TAGS</div>
                                </div>
                                <div class="content-widget">
                                    <?php foreach(explode(',',$detail->tags) as $e): ?>
                                            <a href="<?= base_url('blog') ?>?direccion=<?= $e ?>" class="tag"><?= $e ?></a>
                                    <?php endforeach ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-2">
                        
                        <div class="col-1">
                            <div class="gallery-widget widget">
                                <div class="title-widget">
                                    <div class="title">GALERIA</div>
                                </div>
                                <div class="content-widget">
                                    <ul class="list-unstyled list-inline">
                                        <?php $this->db->order_by('id','DESC') ?>
                                        <?php $this->db->limit(8) ?>
                                        <?php $galeria = $this->db->get('galeria'); ?>
                                            <?php foreach($galeria->result() as $p): ?>
                                                <li>                                                                                                      
                                                    <div style="width:100px; height:100px; background:url(<?= base_url() ?>img/galeria/<?= $p->foto ?>); background-size:auto 100%; background-position:center;" class="img-responsive"></div>                                                        
                                                </li>
                                        <?php endforeach ?>   
                                    </ul>
                                </div>
                            </div>
                            <div class="social-widget widget">
                                <div class="title-widget">
                                    <div class="title">SOCIAL</div>
                                </div>
                                <div class="content-widget">
                                    <ul class="list-unstyled list-inline">
                                        <li>
                                            <a href="javascript:void(0)" class="social-icon fa fa-facebook"></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)" class="social-icon fa fa-twitter"></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)" class="social-icon fa fa-instagram"></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)" class="social-icon fa fa-google"></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)" class="social-icon fa fa-rss"></a>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>