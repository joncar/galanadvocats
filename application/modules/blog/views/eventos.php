<?php 
	$view = $this->load->view('activitats',array(),TRUE,'paginas');
	$this->db->order_by('fecha','ASC');
	$eventos = $this->db->get_where('eventos',array('fecha >='=>date("Y-m-d")));	
	if($eventos->num_rows()>0){
		foreach($eventos->result() as $n=>$v){
			$eventos->row($n)->foto = base_url('img/eventos/'.$v->foto);
			$eventos->row($n)->descripcion_corta = strip_tags($v->descripcion_corta);
		}
		$view = $this->querys->fillFields($view,array('eventos'=>$eventos));	
		$eventos = $this->db->get_where('eventos',array('fecha >='=>date("Y-m-d"),'id !='=>$eventos->row()->id));	
		foreach($eventos->result() as $n=>$v){
			$eventos->row($n)->foto_grande = base_url('img/eventos/'.$v->foto_grande);
			$eventos->row($n)->descripcion = strip_tags($v->descripcion);
		}
		foreach($eventos->row() as $n=>$v){			
			$view = str_replace('['.$n.']',$v,$view);
		}
	}

	echo $view;
?>